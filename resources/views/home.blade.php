<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Fake-Images - Simple image placeholders</title>

  <link rel="stylesheet" href="/css/style.css">
</head>
<body>
  <div class="wrapper">
    <header>
      <p>
        Welcome to <strong>Fake-Images</strong>
      </p>
      <p>
        A simple <a href="http://placehold.it" target="_blank">placehold.it</a> clone.
      </p>
    </header>

    <main>
      <div id="how_to_use">
        <p>Just put your image size after our URL, like this :</p>
        <p class="example">
          <a href="{{ url('300x200') }}">{{ url('300x200') }}</a><br />
          <a href="{{ url('300') }}">{{ url('300') }}</a> // square image
        </p>
        <p>
          You can also use it in your HTML code :
        </p>
        <p class="example">
          &lt;img src="{{ url('300x200') }}"&gt;
        </p>
      </div>
    </main>

    <footer>
      <p>
        <small>Source code on <a href="#">GitHub</a></small>
      </p>
    </footer>
  </div>
</body>
</html>