<?php

namespace App\Http\Controllers;

use Image;

class ImageController extends Controller
{
    /**
     * Generate an image
     *
     * @param integer $w Width of the image
     * @param integer $h Height of the image
     * @param string  $bg_color Optional Background-color of the image
     * @param string  $font_color Optional Font color
     */
    public function generate($w, $h, $bg_color = '#2c3e50', $font_color = '#bdc3c7')
    {
        $img = Image::canvas($w, $h, '#2c3e50');
        $text = "{$w}x{$h}";

        $img->text($text, $w / 2, $h / 2, function($font) {
            $font->file(app()->basePath() . '/public/fonts/OpenSans-Bold.ttf');
            $font->size('26');
            $font->color('#bdc3c7');
            $font->align('center');
            $font->valign('middle');
        });

        return $img->response('png');
    }

    /**
     * Generate a square image
     *
     * @param integer $size Size of the image
     */
    public function square($size)
    {
        return $this->generate($size, $size);
    }
}
